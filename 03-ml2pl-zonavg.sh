#!/bin/bash

set -eu

# change to working directory

cd /scratch/m/m300859/wap2w_ncl/splitted

daily_files=$(ls -1 day??_N96.nc)

ml2pl () {
    levels="100000,92500,85000,77500,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000,700,500,300,200,100,70,50,30,20,10,7,5,3,2,1,0.7,0.5,0.3,0.2,0.1,0.07,0.05,0.03,0.02,0.01"
    input=$1
    output=${input/\.nc/_p.nc}
    rm $output
    cdo -f nc5 ml2pl,${levels} $input $output
}
export -f ml2pl
#parallel -v -j 8 ml2pl ::: $daily_files

daily_files=$(ls -1 day??_N96_p.nc)

rm -f w_N96_p.nc
cdo -f nc5 mergetime $daily_files w_N96_p.nc

rm -f ../zonavg.nc
cdo -f nc5 zonavg w_N96_p.nc ../zonavg.nc

exit
