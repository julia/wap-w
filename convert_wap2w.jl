#----------------------------------------------------------------------------
# Developed by Volker Neff based on a script by Luis Kornblueh
# February 2021
#----------------------------------------------------------------------------



# load local modules
using Dates
using Dispatcher
using cdi.cdiDatasets
using cdi
import Base.OneTo
using Base.Threads

using Strided

############################### config ################################

# declare config type on all wokres 
Config = Dict{String,Any}

const config = Config()

config["input_dir"] = "/scratch/m/m300859/wap2w_julia" |> normpath
config["output_dir"] = "/scratch/m/m300859/wap2w_julia" |> normpath

config["input_temperature"] = joinpath(config["input_dir"], "temperature_ml.grb")
config["output_temperature"] = joinpath(config["output_dir"], "temperature_ml_N96.grb")

config["input_vertical_velocity"] = joinpath(config["input_dir"], "vertical_velocity_ml.grb")
config["output_vertical_velocity"] = joinpath(config["output_dir"], "vertical_velocity_ml_N96.grb")

config["input_surface_pressure"] = joinpath(config["input_dir"], "surface_pressure_ml.grb")
config["output_surface_pressure"] = joinpath(config["output_dir"], "surface_pressure_ml_N96.grb")

config["zonal_avg"] = joinpath(config["output_dir"], "zonavg.nc")


####################### export global functions ####################### 

# p3d = hyam + hybm * aps
function calculate_pressure(aps::Array{Float64}, 
                            hyam::Array{Float64},  
                            hybm::Array{Float64})    

    p3d = Array{Float64, 3}(undef, size(aps)..., length(hyam))
    @threads for k in eachindex(hyam)
        @.p3d[:,:,k] = hyam[k] + hybm[k] * aps[:,:]
    end                              
    
    return p3d
end

const RD    = 287.058        # gas constant for dry air [J/K/kg]
const G     = 9.80665       # gravity acceleration [m/s^2]

"""
Input:
- `p3d`: A variable of any dimensionality containing omega (Pa/s) Array size and shape must match p and t. 
- `t`: A variable containing temperature (K). Array size and shape must match p3d and p.
- `p`: A variable containing pressure values (Pa). Array size and shape must match p3d and t. 
"""
function convert_wap2w( omega::Array{Float64}, 
                        p::Array{Float64},                   
                        t::Array{Float64})
    w = similar(p)
    rho = similar(p)

    @strided @. rho = p/(RD*t)
    @strided @. w = -omega/(rho*G)

    return w
end

function convert(inputPath_t, inputPath_w, inputPath_ps, outputPath)
    input_t = cdiDataset(inputPath_t)
    input_w = cdiDataset(inputPath_w)
    input_ps = cdiDataset(inputPath_ps)

    var_w = Variable("w", grid(input_t["t"]), zAxis(input_t["t"]), TIME.VARYING, unit = "m s-1", stdName = "upward_air_velocity")
    var_aps = Variable("aps", grid(input_ps["var134"]), zAxis(input_ps["var134"]), TIME.VARYING, unit = "Pa", stdName = "surface_air_pressure")

    output = cdiDataset(outputPath, FileTypes.FILETYPE_NC, TAxisDuplicateTemplate(tAxis(input_t)), [var_w, var_aps])

    while getNextTimestep(input_t) &&
        getNextTimestep(input_w) &&
        getNextTimestep(input_ps)
        # load data 
        wap = loadData(input_w["w"])
        t = loadData(input_t["t"])
        ps = loadData(input_ps["var134"])

        z = zAxis(input_w["w"])

        p = calculate_pressure(ps[:, :, 1], hyam(z), hybm(z))

        wz = convert_wap2w(wap, p, t)

        # set new timestep
        dt = DateTime(input_w)
        println("Handel " *  Dates.format(dt, "yyyy-mm-dd HH:MM:SS"))
        setNextTimestep!(output, dt)
        
        # write data
        storeData!(output["aps"], ps)
        storeData!(output["w"], wz)
    end

    return outputPath
end

function cdo_reduce(inputFile, outputFile, varname)
    if !isfile(outputFile)
        run(`cdo -f grb2 -P 12 sp2gp,linear -sp2sp,191 -selname,$(varname) $(inputFile) $(outputFile)`)
    end

    return outputFile
end

function cdo_remap(inputFile, outputFile)
    if !isfile(outputFile)
        run(`cdo -R -f grb2 -P 8 remapcon,N96 $(inputFile) $(outputFile)`)
    end

    return outputFile
end

function cdo_mearge(outputFile, inputFiles...)
    if !isfile(outputFile)
        in = join(inputFiles, "")
        run(`cdo -f nc5 merge $(in) $(outputFile)`)
    end

    return outputFile
end

function cdo_zonavg(inputFiles, outputFile) 
    if !isfile(outputFile)
        run(`cdo -f nc5 zonavg  $(inputFiles) $(outputFile)`)
    end

    return outputFile
end

####################### Main ####################### 

# start time of processing
@info "Start processing: " * Dates.format(now(), "yyyy-mm-dd HH:MM:SS")
@info "Use " * string(Threads.nthreads()) * " threads for processing"
#@info "Use " * string(nprocs()) * " processes for processing"
#@info "Use " * string(nworkers()) * " workers for processing"

mkpath(config["output_dir"])

tem = @op cdo_reduce(config["input_temperature"], config["output_temperature"], "t")
vv  = @op cdo_reduce(config["input_vertical_velocity"], config["output_vertical_velocity"], "w")
p   = @op cdo_remap(config["input_surface_pressure"], config["output_surface_pressure"])

#x = @op cdo_mearge(joinpath(config["output_dir"], "prepare_for_processing.nc"), tem, vv, p)

y = @op convert(tem, vv, p, joinpath(config["output_dir"], "after_processing.nc"))

z = @op cdo_zonavg(y, config["zonal_avg"])

run!(AsyncExecutor(), [z])

@info "Finish processing: " * Dates.format(now(), "yyyy-mm-dd HH:MM:SS")