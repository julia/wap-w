# WAP2W

# Installation
## mistral
### Installation of cdi.jl
he following guide show you how to install the original libcdi followed by cdi.jl on mistral. To follow this guide you have to run the commands on a login node.

1. Load necessary modules
```bash
. /sw/rhel6-x64/etc/profile.mistral
module use /sw/spack-rhel6/spack/modules/linux-rhel6-haswell
module load libtool autoconf automake gcc/7.1.0
```

2. Clone the libcdi repository to your local account
```bash
git clone https://gitlab.dkrz.de/mpim-sw/libcdi.git
cd libcdi
git checkout develop
./autogen.sh
```

3. Create a folder where the final library would save. A typical place should be `/pf/m/[YOUR USER]/local`. **You must change `[YOUR USER]`!** This is used in the following guide. If you want to use a different location, please change the path.
```bash
mkdir ~/local
autoreconf -i
./configure --prefix="/pf/m/[YOUR USER]/local" --with-netcdf=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64 --with-eccodes=/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64
make
make install
```

4. Export the newly created library to the `LD_LIBRARY_PATH` system variable. If you want to use cdi.jl in the future again, it could be a good idea to write the following lines into your `~/.bashrc` file. If you did these, do not forget to reload the config with `source ~/.bashrc`.
**You must change `[YOUR USER]`!**
``` bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pf/m/[YOUR USER]/local/lib
```

### Installation of wap2w
``` bash
cd ~
module add julia/1.5.2-gcc-9.1.0
git clone https://gitlab.dkrz.de/julia/wap-w.git
cd wap-w
julia --project=@. -e "import Pkg; Pkg.instantiate()"
```

# Configure
Please change the parameter at the "convert_wap2w.jl" file to change the input and output parameters of the Pipeline. A section of the config is shown below:
``` julia
config["input_dir"] = "/scratch/m/m300859/wap2w" |> normpath
config["output_dir"] = "/scratch/m/m300859/wap2w" |> normpath

config["input_temperature"] = joinpath(config["input_dir"], "temperature_ml.grb")
config["output_temperature"] = joinpath(config["output_dir"], "temperature_ml_N96.grb")

config["input_vertical_velocity"] = joinpath(config["input_dir"], "vertical_velocity_ml.grb")
config["output_vertical_velocity"] = joinpath(config["output_dir"], "vertical_velocity_ml_N96.grb")

config["input_surface_pressure"] = joinpath(config["input_dir"], "surface_pressure_ml.grb")
config["output_surface_pressure"] = joinpath(config["output_dir"], "surface_pressure_ml_N96.grb")
```

# Execute
## mistral
Export the cdi.jl files:
**You must change `[YOUR USER]`!**
``` bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/pf/m/[YOUR USER]/local/lib
```

Execute the TEM-Analyse-Pipeline on mistral
``` bash
cd ~
cd wap-w
srun --partition=compute --nodes=1 --account=mh0287 --time=01:00:00 --output=wap2w.o%j --error=wap2w.e%j --job-name=wap2w julia -t4 --project=@. convert_wap2w.jl
```