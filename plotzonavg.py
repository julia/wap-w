import matplotlib.pyplot as plt
import numpy as np
import xarray as xr


ds_w = xr.open_dataset("zonavg2.nc")
wp = ds_w.w.isel(time=1, lon=0).copy()

#print(ds_w.keys())

levels = np.arange(-0.08,0.0801,0.01)
plt.figure(figsize=(9,4))
p = wp.plot.contourf(levels=levels)
plt.yscale('log')
plt.gca().invert_yaxis()
plt.show()
