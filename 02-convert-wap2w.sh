#!/bin/bash

set -eu

# change to working directory

cd /scratch/m/m300859/wap2w_ncl

if [[ ! -d splitted ]]
then
    mkdir splitted
    cd splitted
    cdo splitday ../prepare_for_processing.nc day
else
    cd splitted
fi

# need to convert to vertical wind in m/s, procedure is following the ncl routine omega_to_w
#
# The solution is an approximation only:
#
#  rd  = 287.058             J/(kg-K) => m2/(s2 K)
#  g   = 9.80665             m/s2
#  rho = p/(rgas*t)          density => kg/m3, array operation
#  w   = -omega/(rho*g)      array operation
#

cat > w.ncl << EOF
begin

  inputFileName = input_file_name
  output_file_name = str_sub_str(input_file_name, ".nc", "_N96.nc")

  inputFile = addfile(inputFileName, "r") ; opens the input file

  ; read invariant variables

  time  = inputFile->time
  lon   = inputFile->lon
  lat   = inputFile->lat
  lev   = inputFile->lev
  hyai  = inputFile->hyai
  hybi  = inputFile->hybi
  hyam  = inputFile->hyam
  hybm  = inputFile->hybm

  nlon  = dimsizes(lon)
  nlat  = dimsizes(lat)  
  nlev  = dimsizes(lev)
  ntime = dimsizes(time)

  outputFileName = output_file_name
  system("rm -f " + outputFileName)
  setfileoption("nc","Format","NetCDF4Classic")
  outputFile = addfile(outputFileName, "c") ; create new file

  outputFile->time = time
  outputFile->lon  = lon
  outputFile->lat  = lat
  outputFile->lev  = lev
  outputFile->hyai = hyai
  outputFile->hybi = hybi
  outputFile->hyam = hyam
  outputFile->hybm = hybm

  ; add code, unit and name information for w

  filevardef(outputFile, "w", "float", (/ "time", "lev", "lat", "lon" /))
  watts = 0.0
  watts@param         = "9.2.0"
  watts@units         = "m s-1"
  watts@long_name     = "upward vertical velocity"
  watts@standard_name = "upward_air_velocity"
  watts@_FillValue    = 1.0e32
  filevarattdef(outputFile, "w", watts)

  filevardef(outputFile, "aps", "float", (/ "time", "lat", "lon" /))
  apsatts = 0.0
  apsatts@param         = "0.3.0"
  apsatts@units         = "Pa"
  apsatts@long_name     = "surface air pressure"
  apsatts@standard_name = "surface_air_pressure"
  apsatts@_FillValue    = 1.0e32
  filevarattdef(outputFile, "aps", apsatts)

  print("Finished new variable definition")

  ; subset for one time slice only

  wz = new((/ nlev, nlat, nlon /), "float")  

  print("Start time loop:")
  wcStrt     = systemfunc("date")

  do t = 0, ntime-1
    print("time step: " + sprinti("%0.3i", t)) 
    wap     = inputFile->w(t,:,:,:)   ; (lev,lat,lon)
    ta      = inputFile->t(t,:,:,:)    ; (lev,lat,lon)
    ps      = inputFile->var134(t,:,:)  ; (lat,lon)

    p = ta

    do k = 0, nlev-1
      p(k,:,:) = doubletofloat(hyam(k)) + doubletofloat(hybm(k))*ps
    end do

    wz = omega_to_w(wap, p, ta)

    print("write aps")
    outputFile->aps(t,:,:) = (/ ps /)
    print("write w")
    outputFile->w(t,:,:,:)    = (/ wz /)

    wcStrtClmP = systemfunc("date")
    wallClockElapseTime(wcStrt, "Processing", 0)

  end do

end

EOF

if [[ -z "$(which ncl)" ]]
then
    module load ncl/6.5.0-gccsys
fi

run_ncl_script () {
    input=$1
    ncl input_file_name=\"${input}\" w.ncl 
}
export -f run_ncl_script

daily_files=$(ls -1 day??.nc)

parallel -v -j 8 run_ncl_script ::: $daily_files

exit

