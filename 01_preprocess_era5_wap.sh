#! /bin/bash

cd /scratch/m/m214089/wap2w

# reduce data to Gaussian grid 384 x 192

if [[ ! -e temperature_ml_N96.grb ]]
then
    cdo -f grb2 -P 12 sp2gp,linear -sp2sp,191 -selname,t temperature_ml.grb temperature_ml_N96.grb &
fi

if [[ ! -e vertical_velocity_ml_N96.grb ]]
then
    cdo -f grb2 -P 12 sp2gp,linear -sp2sp,191 -selname,w vertical_velocity_ml.grb vertical_velocity_ml_N96.grb &
fi

if [[ ! -e surface_pressure_ml_N96.grb ]]
then
    cdo -R -f grb2 -P 8 remapcon,N96 surface_pressure_ml.grb  surface_pressure_ml_N96.grb &
fi

wait

if [[ ! -e prepare_for_processing.nc ]]
then
    cdo -f nc5 merge surface_pressure_ml_N96.grb vertical_velocity_ml_N96.grb temperature_ml_N96.grb prepare_for_processing.nc
fi

exit
